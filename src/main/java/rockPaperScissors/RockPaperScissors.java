package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;


public class RockPaperScissors {
	

    //Main here, implement the methods vv
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */

         
        new RockPaperScissors().run();
    }
  
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 0;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");



    public String randomChoice(){
        Random choiceInteger = new Random();
        int x = choiceInteger.nextInt(2);
        String actionChoice = rpsChoices.get(x);
        
                return actionChoice ;


    }
         
    public String isWinner(String choice1,String choice2){
        if (choice1.equals("paper") && choice2.equals("rock")){
                return "paper";
            
         }else if (choice1.equals("scissors") && choice2.equals("paper")){
                return "scissors";

        }else if (choice1.equals("rock") && choice2.equals("scissors")){
            return "rock";

        }else{
            return "hello there :)... General Kenobi 0_0" ;
        }


    }

    public String userChoice(){
        while(true){
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String humanChoice = sc.nextLine().toLowerCase();
            if(humanChoice.equals("rock") || humanChoice.equals("paper") || humanChoice.equals("scissors")){

            return humanChoice;

            }else{
                System.out.println("I don't understand" + humanChoice+". Try again");
            }

            

            
            
        }

        


    }

    public String continuePlaying(){
        while(true){

           String continueAnswer = sc.nextLine().toLowerCase();
           if(continueAnswer.equals("y")|| continueAnswer.equals("n")){
           
            return continueAnswer;
        }else{
            System.out.println("I don't understand" + continueAnswer+". Try again");


        }
           

        }


    }

 
    
    public void run() {

        while(true){
            roundCounter+=1;

            System.out.println("Let's play round "+roundCounter);
            String humanChoice=userChoice();
            String computerChoice=randomChoice();
            String choiceString="Human chose "+humanChoice+", computer chose "+computerChoice+".";

            if(isWinner(humanChoice, computerChoice).equals(humanChoice)){
                System.out.println(choiceString + " Human wins.");
                humanScore += 1;
            
            }else if(isWinner(computerChoice,humanChoice).equals(computerChoice)){

                System.out.println(choiceString+" Computer wins.");
                computerScore +=1;

            }else{
                System.out.println(choiceString+" It's a tie");

            }
            System.out.println("Score: human "+humanScore+" computer "+computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");


            String continueAnswer = continuePlaying();
            if(continueAnswer.equals("n")){
                System.out.print("Bye bye :)");
            break;
            } 
            




            }



        }

    





    

    


    //Run
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
